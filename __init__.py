'''
Created by Csaba Csuzdi
Hacktivity 2019
'''
import badge
import random
import samd
import system
import time
import ugfx


GUN_WIDTH = 16
GUN_HEIGHT = 16
MOVEMENT = 8
ASTEROID_WIDTH = 8
ASTEROID_HEIGHT = 2
ASTEROID_CNT = 2
BULLET_SIZE = 2

state = {
    'pos': 64-GUN_WIDTH//2,
    'bullets': [],
    'asteroids': [],
    'points': 0,
    'life': 6,
    'music': False,
    'end': False
}


def music():
    while True:
        if state['end']:
            return
        if state['music']:
            samd.buzzer(random.randrange(1, 8), 1)


def draw_gun():
    x = state['pos']
    ugfx.area(x, 64-GUN_HEIGHT//2, GUN_WIDTH, GUN_HEIGHT//2, 1)
    ugfx.area(x+GUN_WIDTH//4, 64-GUN_HEIGHT, GUN_WIDTH//2, GUN_HEIGHT//2, 1)


def draw_object(obj, w, h):
    ugfx.area(obj['x'], obj['y'], w, h, 1)


def draw_objects(objects, w, h):
    for obj in objects:
        draw_object(obj, w, h)


def draw_point(point):
    ugfx.string_box(100, 0, 128-100, 10, str(point),
                    "Roboto_Regular12", ugfx.BLACK, ugfx.justifyRight)


def move(direction):
    x = state['pos'] + MOVEMENT*direction
    x = max(0, x)
    x = min(128-GUN_WIDTH, x)
    state['pos'] = x


def right(pushed):
    if not pushed:
        return
    move(1)


def left(pushed):
    if not pushed:
        return
    move(-1)


def fire(pushed):
    if not pushed:
        return
    state['bullets'].append({
        'x': state['pos'] + GUN_WIDTH//2,
        'y': 64-GUN_HEIGHT
    })


def action_exit(pushed):
    if (pushed):
        badge.led(6, 0, 0, 0)
        state['end'] = True
        system.home()


def step_objects(objs, direction):
    for obj in objs:
        obj['y'] = obj['y']+1*direction
    new_objs = [obj for obj in objs if obj['y'] >= 0 and obj['y'] < 64]
    objs.clear()
    objs.extend(new_objs)


def generate_asteroids():
    l = len(state['asteroids'])
    if l >= ASTEROID_CNT:
        return
    for i in range(ASTEROID_CNT-l):
        state['asteroids'].append({
            'x': random.randrange(0, 128-ASTEROID_WIDTH),
            'y': i
        })


def hit():
    state['points'] += 1
    badge.led(6, 1, 0, 0)


def is_collide(o1x, o1y, o1w, o1h, o2x, o2y, o2w, o2h):
    return (o1x < o2x + o2w and
            o1x + o1w > o2x and
            o1y < o2y + o2h and
            o1y + o1h > o2h)


def collide():
    newbullets = state['bullets'][:]
    newasteroids = state['asteroids'][:]
    for bullet in state['bullets']:
        for asteroid in state['asteroids']:
            if is_collide(bullet['x'], bullet['y'], BULLET_SIZE, BULLET_SIZE,
                          asteroid['x'], asteroid['y'], ASTEROID_WIDTH, ASTEROID_HEIGHT):
                if bullet in newbullets:
                    newbullets.remove(bullet)
                if asteroid in newasteroids:
                    newasteroids.remove(asteroid)
                hit()
    state['bullets'] = newbullets
    for asteroid in state['asteroids']:
        if (is_collide(state['pos'], 64-GUN_HEIGHT//2, GUN_WIDTH, GUN_HEIGHT//2,
                       asteroid['x'], asteroid['y'], ASTEROID_WIDTH, ASTEROID_HEIGHT) or
            is_collide(state['pos']+GUN_WIDTH//4, 64-GUN_HEIGHT, GUN_WIDTH//2, GUN_HEIGHT//2,
                       asteroid['x'], asteroid['y'], ASTEROID_WIDTH, ASTEROID_HEIGHT)):
            if asteroid in newasteroids:
                newasteroids.remove(asteroid)
            state['life'] -= 1
            if state['life'] <= 0:
              state['end'] = True
    state['asteroids'] = newasteroids


def step():
    step_objects(state['bullets'], -1)
    step_objects(state['asteroids'], 2)
    collide()
    generate_asteroids()


def update_life(cnt):
    for i in range(6):
        if i < cnt:
            badge.led(i, 0, 0, 1)
        else:
            badge.led(i, 0, 0, 0)


def game_over():
    ugfx.string(10, 20, "Game Over", "Roboto_Regular18", 1)
    ugfx.flush()


def main():
    badge.init()
    ugfx.input_init()
    ugfx.input_attach(ugfx.BTN_B, action_exit)
    ugfx.input_attach(ugfx.JOY_RIGHT, right)
    ugfx.input_attach(ugfx.JOY_LEFT, left)
    ugfx.input_attach(ugfx.JOY_UP, fire)
    # _thread.start_new_thread("music", music, ())
    while not state['end']:
        ugfx.clear()
        step()
        draw_gun()
        draw_objects(state['bullets'], BULLET_SIZE, BULLET_SIZE)
        draw_objects(state['asteroids'], ASTEROID_WIDTH, ASTEROID_HEIGHT)
        draw_point(state['points'])
        ugfx.flush()
        update_life(state['life'])
    game_over()
    while True:
        time.sleep(1)


main()
